



$(document).ready(function() {


    //add avancement

     $(document).on('click','#BTN_AVANCEMENT',function(){  
    

 var av= $("#avancement").val();
 var odtnum= $("#AVodtN").val();
 var percent= $("#AVpercent").val();
 
 
 $("#avancement").css("background","rgba(45, 195, 19, 0.89)");
 
 $("#AVpercent").css("background","rgba(45, 195, 19, 0.89)");
 

     $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['AVANCEMENT', {
            'O_AVANCEMENT': av,'O_AV_PERCENT':percent},odtnum])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);
     }).done(function(data) {
       console.dir(data);  
         
        });

});
////get session user /////////////
$.post('/limitless/layout_5/LTR/default/sessioninfo.php', JSON.stringify(['username'])).fail(function(data) {
        
        console.dir(data);
    }).done(function(data) {
        console.dir(data);

    });

////get the number of notifications /////////////
$.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['NOTIF_NUM',])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);
    }).done(function(data) {
        var notifnum=data[0].notifnum;
        
        $('#notifnum').append('<p id="notifcount">'+notifnum+'</p>');
        if (notifnum>0) {
        $('#bell').addClass('shakeit');
                        }

    });
////update avancement



////// changing the status of a notif grom unread to read
$(document).on('click','#readnotif',function()
{

     var classit= parseInt($(this).children("i").attr("class").split('f')[1]);
     var checkid=".check"+classit.toString();
     $(checkid).show();
 var notifcount=document.getElementById("notifcount").innerHTML;
 $(this).parent().remove();
 if (notifcount==1) {
    $('#bell').removeClass('shakeit');
 }
 $('#notifnum').children("p").remove();
$('#notifnum').append('<p id="notifcount">'+(notifcount-1)+'</p>');

     $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['READ_NOTIF', {
            'N_STATUS': "READ"},classit])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);
     }).done(function(data) {
       console.dir(data);  
         
        });
        
});



/////getting the notifications when clicking on the bell
    $(document).on('click','#notifopen',function(){


    
    $('#notifications').children("li").remove();
  

  $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['GET_NOTIF','UNREAD'])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);
    }).done(function(data) {
        data.forEach(function(element) {
          console.dir(element);
            $('#notifications').append('<li class="media" style="text-transform: uppercase;"><div class="media-left"><a href="#" class="label label-striped border-left-teal-300 label-icon"><i class=" icon-bubble-notification"></i></a></div><div class="media-body"><a href="#">'+element.N_SENDER+'</a> APROPS <a href="#" style="color: red;">'+element.N_TOPIC+'</a><a href="#" style="color: blue;float:right;" id="readnotif" class="label label-flat border-success text-success-600"><span ><i style="Display:none;" class="  icon-bell-check'+element.N_ID+'"></i>Vu</span><i class="i icon-bell-check notif'+element.N_ID+'"></i></a><div class="media-annotation">'+element.N_MESSAGE+'</div></div></li>');
            
            
        });
        
    });
});

///show the notification form
$(document).on('click','#Notifier',function(){

$('#notificationsubmitform').show();
});
  /////// hide the notification  form 
$(document).on('click','#close',function(){
$('#notificationsubmitform').hide();
});

/////saving notification in db
$(document).on('click','#notifsubmit',function(){
 $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['ADD_NOTIF', {
            'N_SENDER': $('#notif_nom').val(),
            'N_TOPIC':$('#notif_sujet').val(),
            'N_MESSAGE': $('#notif_msg').val(),
            'N_STATUS': ("UNREAD"),
            'N_RECEIVER': ("CDT")
            
             
        }])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            //console.log(data);
            $('#notificationsubmitform').hide();
            $('#notifcheck').show();

            setTimeout(function () {
            $('#notifcheck').hide();
            }, 6000);
            
           /* $('#TAB_MIG').DataTable().ajax.reload();*/
        });
    });



 if (window.location.pathname == '/limitless/layout_5/LTR/default/stat.html') {



///get statistiques from database 
 $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_STAT',])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);
    }).done(function(data) {
       console.dir(data);
       var totalnum=(data[0][0].totalnum);
       var offnum=(data[1][0].offnum);
       var sousoffnum=(data[2][0].sousoffnum);
       var hommedetroupnum=totalnum-offnum-sousoffnum;
       var malenum =(data[3][0].malenum);
       var femalenum=totalnum-malenum;
       var young=(data[4][0].young);
       var old=(data[5][0].old);
       var adult=(totalnum-young-old);
     
////// first bar chart effectif par nombre
           var bar_chart = c3.generate({
        bindto: '#c3-bar-chart',
        size: { height: 324 },
        data: {
            columns: [
                ['Officier', offnum],
                ['Sous Off', sousoffnum],
                ['homme de troupe', hommedetroupnum]
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#2196F3', '#FF9800', '#4CAF50']
        },
        bar: {
            width: {
                ratio: 0.5
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });



    // Pie chart
    // ------------------------------

    // Generate chart
    ///effectif par %
    var pie_chart = c3.generate({
        bindto: '#c3-pie-chart',
        size: { width: 350 },
        color: {
            pattern: ['#3F51B5', '#FF9800', '#4CAF50', '#00BCD4', '#F44336']
        },
        data: {
            columns: [
                 ['Officier', offnum],
                ['Sous Off', sousoffnum],
                ['homme de troupe', hommedetroupnum]
            ],
            type : 'pie'
        }
    });


 var bar_chart = c3.generate({
        bindto: '#piex',
        size: { height: 324 },
        data: {
            columns: [
                ['M', malenum],
                ['F', femalenum],
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#3F51B5', '#fb00ff', '#4CAF50']
        },
        bar: {
            width: {
                ratio: 0.5
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });



    // Pie chart
    // ------------------------------
/////////effectif par sex
    // Generate chart
    var pie_chart = c3.generate({
        bindto: '#effectifagepercent',
        size: { width: 350 },
        color: {
            pattern: ['#3F51B5', '#fb00ff', '#4CAF50', '#00BCD4', '#F44336']
        },
        data: {
            columns: [
                ['<30ans', young],
                ['30->40ans', adult],
                ['>40ans', old],
            ],
            type : 'pie'
        }
    });



var bar_chart = c3.generate({
        bindto: '#pieage',
        size: { height: 324 },
        data: {
            columns: [
                ['<30ans', young],
                ['25->40ans', adult],
                ['>40ans', old],
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#3F51B5', '#fb00ff', '#4CAF50']
        },
        bar: {
            width: {
                ratio: 0.5
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });



    // Pie chart
    // ------------------------------
/////////effectif par sex
    // Generate chart
    var pie_chart = c3.generate({
        bindto: '#effectifpercent',
        size: { width: 350 },
        color: {
            pattern: ['#3F51B5', '#fb00ff', '#4CAF50', '#00BCD4', '#F44336']
        },
        data: {
            columns: [
                ['M', malenum],
                ['F', femalenum],
            ],
            type : 'pie'
        }
    });
        });
    
}


   



if (window.location.pathname == '/limitless/layout_5/LTR/default/effectifajout.html') {
//bootstrap-uploader
        $("#images").fileinput({
        uploadAsync: false,
        uploadUrl: "/limitless/layout_5/LTR/default/upload.php", // your upload server url
        uploadExtraData: function() {
            return {
                username: $("#imagecaption").val()
            };
        }
    });
//bootstrap-uploader

       // Override defaults
    $.fn.selectpicker.defaults = {
        iconBase: '',
        tickIcon: 'icon-checkmark3'
    }
    // Basic setup
    // ------------------------------

    // Basic select
    $('.bootstrap-select').selectpicker();

}
//// Display all information of personnel
$(document).on('click','#voirpluspersonnel',function(){
   // alert($(this).attr('name')); 

    $('#infopopup').show();
    $('#popupimage').children("img").remove();
    $('#popupname').children("p").remove();
    $('#pupopbd').children("p").remove();
    $('#popuplocation').children("p").remove();
    $('#popupengagement').children("p").remove();

    $('#popupdiplome').children("p").remove();
    $('#popupniveau').children("p").remove();
    $('#popuplangue').children("p").remove();
    $('#popupdateengagement').children("p").remove();

    $('#popupdatepassage').children("p").remove();
    $('#popupdatefin').children("p").remove();
    $('#popuplocation').children("p").remove();
    $('#popupanciennete').children("p").remove();

    $('#popupspec').children("p").remove();
    $('#popupservice').children("p").remove();
    $('#popupfamily').children("p").remove();
    $('#popupenfant').children("p").remove();

  $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_ONE_PERSONNEL',$(this).attr('name')])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);
    }).done(function(data) {
        data.forEach(function(element) {
          console.dir(element);
            $('#popupimage').append('<img style="margin-top:15px;height: 150px;" src="uploads/'+element.P_IMAGE+'.jpg" class="" alt="">');
            $('#popupname').append('<p style="text-transform: uppercase;margin-left:34px;padding-left:10px;border-left: 1px solid #71bcf7;">'+element.P_NAME+'</p>');
            $('#popupname').append('<p><img style="margin-top:-40px;max-height:42px;margin-right:17px;float:left;" src="image/'+element.P_GRADE+'.png" class="" alt=""></p>');
            $('#pupopbd').append('<p style="text-transform: uppercase;color:black;">'+element.P_DATEDEN+'</p>');
            $('#popuplocation').append('<p style="text-transform: uppercase;color:black;">'+element.P_LIEUDEN+'</p>');
            $('#popupengagement').append('<p style="text-transform: uppercase;color:black;">'+element.P_DATEENGAG+'</p>');

            $('#popupdiplome').append('<p style="text-transform: uppercase;color:black;">'+element.P_DIPLOMELORSREC+'</p>');
            $('#popupniveau').append('<p style="text-transform: uppercase;color:black;">'+element.P_NIVEAUACTUEL+'</p>');
            $('#popuplangue').append('<p style="text-transform: uppercase;color:black;">'+element.P_LANG+'</p>');
            $('#popupdateengagement').append('<p style="text-transform: uppercase;color:black;">'+element.P_DATEENGAG+'</p>');

            $('#popupdatepassage').append('<p style="text-transform: uppercase;color:black;">'+element.P_DATENOMI+'</p>');
            $('#popupdatefin').append('<p style="text-transform: uppercase;color:black;">'+element.P_DATEFINCONT+'</p>');
            $('#popupanciennete').append('<p style="text-transform: uppercase;color:black;">'+element.P_DATEFINCONT+'</p>');

            $('#popupspec').append('<p style="text-transform: uppercase;color:black;">'+element.P_SPECIALITE+'</p>');
            $('#popupservice').append('<p style="text-transform: uppercase;color:black;">'+element.P_SERVICE+'</p>');
            $('#popupfamily').append('<p style="text-transform: uppercase;color:black;">'+element.P_ETATFAM+'</p>');
            $('#popupenfant').append('<p style="text-transform: uppercase;color:black;">'+element.P_NBRENFANT+'</p>');
        });
        
    });
});


/////Perform search by service
    $(document).on('click','#BTN_SEARCHBYSERVICE',function(){
    //alert($(this).attr('name')); 
    

  $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_BY_SERVICE',$(this).attr('name')])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        $('#effectif_list').children(".media").remove();
        data.forEach(function(element) {
          console.dir(element);
           $('#effectif_list').append('<li class="media" style="display: -webkit-box;"><div class="media-left" style="display: inline;"></a><img style="margin-top:15px;height:60px;margin-right:20px;" src="image/'+element.P_GRADE+'.png" class="" alt=""><a href="#"><img style="margin-top:15px;width:55px;height:55px;" src="uploads/'+element.P_IMAGE+'.jpg" class="img-circle" alt=""></div><div class="media-body"><h3 class="media-heading">'+element.P_NAME+'</h3>' + element.P_SERVICE + '</br>'+element.P_PHONE+' <button style="float: right;right: 135px;margin-top: -30px;" type="button" id="voirpluspersonnel" name="'+element.P_NAME+'" class="btn btn-primary btn-icon btn-rounded"><i class=" icon-arrow-right13"></i>Voir Plus</button> </div></li></p>');
        });
        
    });


});

    /////Perform search by sex
    $(document).on('click','#BTN_SEARCHBYSEX',function(){
    //alert($(this).attr('name')); 
    

  $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_BY_SEX',$(this).attr('name')])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        $('#effectif_list').children(".media").remove();
        data.forEach(function(element) {
          console.dir(element);
           $('#effectif_list').append('<li class="media" style="display: -webkit-box;"><div class="media-left" style="display: inline;"></a><img style="margin-top:15px;height:60px;margin-right:20px;" src="image/'+element.P_GRADE+'.png" class="" alt=""><a href="#"><img style="margin-top:15px;width:55px;height:55px;" src="uploads/'+element.P_IMAGE+'.jpg" class="img-circle" alt=""></div><div class="media-body"><h3 class="media-heading">'+element.P_NAME+'</h3>' + element.P_SERVICE + '</br>'+element.P_PHONE+' <button style="float: right;right: 135px;margin-top: -30px;" type="button" id="voirpluspersonnel" name="'+element.P_NAME+'" class="btn btn-primary btn-icon btn-rounded"><i class=" icon-arrow-right13"></i>Voir Plus</button> </div></li></p>');
        });
        
    });


});
        /////Perform search by EF
    $(document).on('click','#BTN_SEARCHBYEF',function(){
    //alert($(this).attr('name')); 

  $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_BY_EF',$(this).attr('name')])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        $('#effectif_list').children(".media").remove();
        data.forEach(function(element) {
          console.dir(element);
           $('#effectif_list').append('<li class="media" style="display: -webkit-box;"><div class="media-left" style="display: inline;"></a><img style="margin-top:15px;height:60px;margin-right:20px;" src="image/'+element.P_GRADE+'.png" class="" alt=""><a href="#"><img style="margin-top:15px;width:55px;height:55px;" src="uploads/'+element.P_IMAGE+'.jpg" class="img-circle" alt=""></div><div class="media-body"><h3 class="media-heading">'+element.P_NAME+'</h3>' + element.P_SERVICE + '</br>'+element.P_PHONE+' <button style="float: right;right: 135px;margin-top: -30px;" type="button" id="voirpluspersonnel" name="'+element.P_NAME+'" class="btn btn-primary btn-icon btn-rounded"><i class=" icon-arrow-right13"></i>Voir Plus</button> </div></li></p>');
        });
        
    });


});

       /////Perform search by #kids
    $(document).on('click','#BTN_SEARCHBYKIDS',function(){
    //alert($(this).attr('name')); 
    
 var kids= document.getElementById('kids').value;
 
  $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_BY_KIDS',kids])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        $('#effectif_list').children(".media").remove();
        data.forEach(function(element) {
          console.dir(element);
           $('#effectif_list').append('<li class="media" style="display: -webkit-box;"><div class="media-left" style="display: inline;"></a><img style="margin-top:15px;height:60px;margin-right:20px;" src="image/'+element.P_GRADE+'.png" class="" alt=""><a href="#"><img style="margin-top:15px;width:55px;height:55px;" src="uploads/'+element.P_IMAGE+'.jpg" class="img-circle" alt=""></div><div class="media-body"><h3 class="media-heading">'+element.P_NAME+'</h3>' + element.P_SERVICE + '</br>'+element.P_PHONE+' <button style="float: right;right: 135px;margin-top: -30px;" type="button" id="voirpluspersonnel" name="'+element.P_NAME+'" class="btn btn-primary btn-icon btn-rounded"><i class=" icon-arrow-right13"></i>Voir Plus</button> </div></li></p>');
        });
        
    });
});

       /////Perform search by SERVICE
    $(document).on('click','#BTN_SEARCHBYSPECIALITE',function(){
    //alert($(this).attr('name')); 
    
 var spe= document.getElementById('specialite').value;
 //alert(spe);
  $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_BY_SPECIALITE',spe])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        $('#effectif_list').children(".media").remove();
        data.forEach(function(element) {
          console.dir(element);
           $('#effectif_list').append('<li class="media" style="display: -webkit-box;"><div class="media-left" style="display: inline;"></a><img style="margin-top:15px;height:60px;margin-right:20px;" src="image/'+element.P_GRADE+'.png" class="" alt=""><a href="#"><img style="margin-top:15px;width:55px;height:55px;" src="uploads/'+element.P_IMAGE+'.jpg" class="img-circle" alt=""></div><div class="media-body"><h3 class="media-heading">'+element.P_NAME+'</h3>' + element.P_SERVICE + '</br>'+element.P_PHONE+' <button style="float: right;right: 135px;margin-top: -30px;" type="button" id="voirpluspersonnel" name="'+element.P_NAME+'" class="btn btn-primary btn-icon btn-rounded"><i class=" icon-arrow-right13"></i>Voir Plus</button> </div></li></p>');
        });
        
    });


});

  /////// hide the popup info  
$(document).on('click','#close',function(){
$('#infopopup').hide();
});

//search personnel
$(document).on('click','#BTN_PERSONNEL_SEARCH',function(){

 //alert('clicked');
        $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_PERSONNEL',$(this).attr('name')])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);
    }).done(function(data) {
        $('#effectif_list').children(".media").remove();
        data.forEach(function(element) {
          console.dir(element);
            $('#effectif_list').append('<li class="media" style="display: -webkit-box;"><div class="media-left" style="display: inline;"></a><img style="margin-top:15px;height:60px;margin-right:20px;" src="image/'+element.P_GRADE+'.png" class="" alt=""><a href="#"><img style="margin-top:15px;width:55px;height:55px;" src="uploads/'+element.P_IMAGE+'.jpg" class="img-circle" alt=""></div><div class="media-body"><h3 class="media-heading">'+element.P_NAME+'</h3>' + element.P_SERVICE + '</br>'+element.P_PHONE+' <button style="float: right;right: 135px;margin-top: -30px;" type="button" id="voirpluspersonnel" name="'+element.P_NAME+'" class="btn btn-primary btn-icon btn-rounded"><i class=" icon-arrow-right13"></i>Voir Plus</button> </div></li></p>');
        });
        
    });
 });


if (window.location.pathname == '/limitless/layout_5/LTR/default/effectifajout.html') {

//// culate 'Enciennete au service'

 var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!

var yyyy = today.getFullYear();
if(dd<10){
    dd='0'+dd;
} 
if(mm<10){
    mm='0'+mm;
} 
var today = dd+'/'+mm+'/'+yyyy;

 document.getElementById("datefincont").addEventListener("keyup", function() {

    var fincontrat=document.getElementById("datefincont").value;
     
    
    var date1=new Date();
    var date2 = new Date(fincontrat);

    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

    document.getElementById("ancien").value = humanise(diffDays); 
     
     
        
     }, false);

};
//ajout odt

$(document).on('click','#BTN_ADD_ODT',function(){

    alert('odt');
         $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['ADD_ODT', {
            'O_N_DEN': $('#onden').val(),
            'O_N_S':$('#ons').val(),
            'O_EQUIPEMENT': $('#oequipement').val(),
            'O_ANOMALIE': $('#oanomalie').val(),
            'O_UNITE': $('#ounite').val(),
            'O_REFERNCE': $('#oreference').val(),
            'O_S_REP': $('#osrep').val(),
            'O_T_DEBUT': $('#otdebut').val(),
            'O_DATE': $('#odate').val(),
            'O_T_FIN': $('#otfin').val()
            
            

             
        }])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            //console.log(data);
            alert('submtted');
            $('#2t').addClass("active");
            $('#1t').removeClass("active");
            location.href =("#panel-pill3")

            
           /* $('#TAB_MIG').DataTable().ajax.reload();*/
        });
});
//rechercheodt

$(document).on('click','#AVgetodt',function(){
    
    var odtnum=$('#AVodtN').val();
   

    $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['GET_ODT',odtnum])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        
        data.forEach(function(element) {
           
             $("#avancement").val(element.O_AVANCEMENT);
            $("#avancement").css("background","rgba(158, 216, 21, 0.3)");

             $("#AVpercent").val(element.O_AV_PERCENT);
            $("#AVpercent").css("background","rgba(158, 216, 21, 0.3)");
var percentage=element.O_AV_PERCENT+'%';
            $("#percentbar").css("width",percentage);


          console.dir(element);
           });
    });

});

$(document).on('click','#getodt',function(){
    
    var odtnum=$('#odtN').val();
   
    $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['GET_ODT',odtnum])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        
        data.forEach(function(element) {
            $("#odtnum").attr('value',element.O_N_DEN);
            $("#odtnum").prop('disabled', true);
            $("#odtnum").css("background","rgba(158, 216, 21, 0.3)");

            $("#equipement").attr('value',element.O_EQUIPEMENT);
            $("#equipement").prop('disabled', true);
            $("#equipement").css("background","rgba(158, 216, 21, 0.3)");

            $("#anomalie").val(element.O_ANOMALIE);
            $("#anomalie").prop('disabled', true);
            $("#anomalie").css("background","rgba(158, 216, 21, 0.3)");

            $("#nombati").attr('value',element.O_UNITE);
            $("#nombati").prop('disabled', true);
            $("#nombati").css("background","rgba(158, 216, 21, 0.3)");

             $("#odtdate").attr('value',element.O_DATE);
            $("#odtdate").prop('disabled', true);
            $("#odtdate").css("background","rgba(158, 216, 21, 0.3)");

            


          console.dir(element);
           });
    });

});
 // Ajouter travaux;
           
  $(document).on('click','#BTN_ADD_TASK',function(){  
  alert('clicked');      
            $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['ADD_TASK', {
            'T_NOMBATI': $('#nombati').val(),
            'T_EQUIPEMENT':$('#equipement').val(),
            'T_MODELE': $('#modele').val(),
            'T_NUMSERIE': $('#numserie').val(),
            'T_DATEMISEENSERVICE': $('#datemiseenservice').val(),
            'T_ANOMALIE': $('#anomalie').val(),
            'T_CONSTAT': $('#constat').val(),
            'T_REMEDE': $('#remede').val(),
            'T_COUT': $('#cout').val(),
            'T_NRNUM': $('#nrnum').val(),
            'T_NRDATE': $('#nrdate').val(),
            'T_ODTNUM': $('#odtnum').val(),
            'T_ODTDATE': $('#odtdate').val(),
            'T_REF': $('#ref').val(),
            'T_REFNUM': $('#refnum').val(),
            'T_REFDIV': $('#refdiv').val(),
            'T_REFDATE': $('#refdate').val(),
            'T_CHEFEQUIPE': $('#chefequipe').val(),
            'T_EQUIPE': $('#equipe').val(),
            'T_BORD': $('#bord').val(),
            'T_DESTIN': $('#destin').val(),
            'T_COPIES': $('#copie').val()
            
            

             
        }])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            //console.log(data);
            alert('submtted');
            $('#submitform').hide();
            
           /* $('#TAB_MIG').DataTable().ajax.reload();*/
        });


////// animating the check icon after submitting
    /*   var transition = $('#anim').data("transition");
        // Add animation class to panel element
        $('#anim').parents(".panel").show();
        $('#anim').parents(".panel").velocity("transition." + transition, { 
            stagger: 1000,
            duration: 1000
        });*/
///////// anim end//////////////////////////////



        });

if (window.location.pathname == '/limitless/layout_5/LTR/default/addtask.html') {

//$("#notifnum").addClass( 'shake');
}


if (window.location.pathname == '/limitless/layout_5/LTR/default/tasklist.html') {

var dataSet=[];


    $.post('/limitless/layout_5/LTR/default/denatphpcontroller.php', JSON.stringify(['SEARCH_TRAVAUX'])).fail(function(data) {
        console.log('fail' + data);
        console.dir(data);

    }).done(function(data) {
        $('#tasklist').children(".task").remove();
        data.forEach(function(element) {
          console.dir(element);
          dataSet.push([ element.T_NRNUM+'/DEN du '+element.T_NRDATE+'<br>'+element.T_ODTNUM+'/DEN du '+element.T_ODTDATE+'<br>'+element.T_REF+' N°  '+element.T_REFNUM+'/'+element.T_REFDIV+' du '+element.T_REFDATE, element.T_NOMBATI, element.T_EQUIPEMENT, element.T_ANOMALIE,element.T_CHEFEQUIPE+'<br>'+'  *'+element.T_EQUIPE, "<button id=\"moretasks\" name="+element.T_ID+" class=\"btn btn-primary\"><i class=\" icon-file-pdf\"></i></button>" ]);

           //$('#tasklist').append('<tr class="task" id="'+element.T_ID+'"><td><ul><li>'+element.T_NRNUM+'/'+element.T_NRDATE+'</li><li>'+element.T_ODTNUM+'/'+element.T_ODTDATE+'</li><li>'+element.T_REF+'/'+element.T_REFNUM+''+element.T_REFDIV+'/'+element.T_REFDATE+'</li></ul></td><td><a>'+element.T_NOMBATI+'</a></td><td>'+element.T_EQUIPEMENT+'</td><td>'+element.T_ANOMALIE+'</td><td><ul><li style="FONT-WEIGHT: 700;">'+element.T_CHEFEQUIPE+'</li> <li style="margin-left: 10px;">'+element.T_EQUIPE+'</li></ul></td><td><span class="label label-success">Active</span></td><td class="text-center"><ul class="icons-list"><li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li><li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li><li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li></ul></li></ul></td></tr>');
        });
        
      $(document).ready(function() {
    $('#example').DataTable( {
        data: dataSet,
        columns: [
            { title: "REFERENCE" },
            { title: "NOM BATI/CORPS" },
            { title: "EQUIPEMENT" },
            { title: "ANOMALIE" },
            { title: "EQUIPE" },
            { title: "IMPRIMER" }

            
        ]
    } );
} );
 $('#example').removeClass("dataTable");
    });
}


// Ajouter travaux;
           
  $(document).on('click','#moretasks',function(){  
    var id=($(this).attr('name')); 
    var win = window.open('/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=odt.mrt&ID='+ id);

    win.focus();

 
});
     


   
 



});
//change #days to years months and days
function humanise (diff) {
  // The string we're working with to create the representation
  var str = '';
  // Map lengths of `diff` to different time periods
  var values = [[' Ans', 365], [' Mois', 30], [' Jours', 1]];

  // Iterate over the values...
  for (var i=0;i<values.length;i++) {
    var amount = Math.floor(diff / values[i][1]);

    // ... and find the largest time value that fits into the diff
    if (amount >= 1) {
       // If we match, add to the string ('s' is for pluralization)
       str += amount + values[i][0] + (amount > 1 ? 's' : '') + ' ';

       // and subtract from the diff
       diff -= amount * values[i][1];
    }
  }

  return str;
}

