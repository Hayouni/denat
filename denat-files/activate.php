<html>
<head>
	<title>Stimulsoft Reports.PHP</title>
</head>
<body>
	Stimulsoft Reports.PHP (JS version) - How to Activate
	<hr><br>

	The Trial version of the product does not contain any restrictions, except for the Trial watermark on the report pages.<br><br>
	
	To activate the product, it is enough to copy the received 'license.key' file to the 'stimulsoft' folder of this project. The license will be loaded automatically. You can add some conditions in the 'license.php' script to load the license file, if it required for security.<br><br>
	
	<a href="index.php">Back</a>
</body>